//
//  EasingAdditionsTests.cpp
//  LightArray
//
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "EasingAdditionsTests.h"
#include "gtest/gtest.h"
#include "easing.h"


static const double floatingPointError = 0.0000001;

TEST(easingValue, normal1){
    
    int totalSteps = 4;//so 5 actual values. 0.0    0.25    0.5     0.75    1.0
    AHFloat start = 0.0;
    AHFloat end = 1.0;
    
    AHFloat (*easeFunc)(AHFloat) = &LinearInterpolation;
    
    for (int curStep = 0; curStep <= totalSteps; curStep++) {
        AHFloat curValue = easingValue(totalSteps, curStep, start, end, easeFunc);
        EXPECT_EQ((AHFloat)curStep/totalSteps, curValue);
    }
    
}

TEST(easingValue, nonZeroStartAndEnd){
    
    int totalSteps = 4;//so 5 actual values.
    AHFloat solutions[5] = {0.2, 0.325, 0.45, 0.575, 0.7};
    AHFloat start = 0.2;
    AHFloat end = 0.7;
    
    AHFloat (*easeFunc)(AHFloat) = &LinearInterpolation;
    
    for (int curStep = 0; curStep <= totalSteps; curStep++) {
        AHFloat curValue = easingValue(totalSteps, curStep, start, end, easeFunc);
        EXPECT_DOUBLE_EQ(solutions[curStep], curValue);
    }
    
}

TEST(easingValue, singleStep){
    int totalSteps = 1;//so 2 actual values. 0.0    1.0
    AHFloat start = 0.0;
    AHFloat end = 1.0;
    
    AHFloat (*easeFunc)(AHFloat) = &LinearInterpolation;
    
    for (int curStep = 0; curStep <= totalSteps; curStep++) {
        AHFloat curValue = easingValue(totalSteps, curStep, start, end, easeFunc);
        EXPECT_EQ((AHFloat)curStep/totalSteps, curValue);
    }
}

void testEaseFuncAndInverse(AHFloat(*easeFunc)(AHFloat), AHFloat(*inverseFunc)(AHFloat)){
    int totalSteps = 1371;
    AHFloat start = 0.0;
    AHFloat end = 1.0;
    
    for (int curStep = 0; curStep <= totalSteps; curStep++) {
        AHFloat dt = (end - start)/(totalSteps);
        AHFloat easeFuncInput = start + dt*curStep;
        AHFloat easeFuncVal = easeFunc(easeFuncInput);
        AHFloat inverseVal = inverseFunc(easeFuncVal);
        ASSERT_NEAR(easeFuncInput, inverseVal, floatingPointError);
    }
    
}


TEST(CubicEaseInInverse, testAgainstManuallyCalculatedValues){
    
    EXPECT_NEAR(CubicEaseInInverse(0.0), 0.0, floatingPointError);
    EXPECT_NEAR(CubicEaseInInverse(1.0), 1.0, floatingPointError);
    EXPECT_NEAR(CubicEaseInInverse(0.125), 0.5, floatingPointError);
    EXPECT_NEAR(CubicEaseInInverse(0.729), 0.9, floatingPointError);
}


TEST(CubicEaseInInverse, testAgainstCubicEaseIn){
    testEaseFuncAndInverse(&CubicEaseIn, &CubicEaseInInverse);
}


TEST(CubicEaseOutInverse, testAgainstManuallyCalculatedValues){
    
    EXPECT_NEAR(CubicEaseOutInverse(0.0), 0.0, floatingPointError);
    EXPECT_NEAR(CubicEaseOutInverse(1.0), 1.0, floatingPointError);
    EXPECT_NEAR(CubicEaseOutInverse(0.578125), 0.25, floatingPointError);
    EXPECT_NEAR(CubicEaseOutInverse(0.999), 0.9, floatingPointError);
}


TEST(CubicEaseOutInverse, testAgainstCubicEaseIn){
    testEaseFuncAndInverse(&CubicEaseOut, &CubicEaseOutInverse);
}

TEST(CubicEaseInOutInverse, testAgainstManuallyCalculatedValues){
    
    EXPECT_NEAR(CubicEaseInOutInverse(0.0), 0.0, floatingPointError);
    EXPECT_NEAR(CubicEaseInOutInverse(1.0), 1.0, floatingPointError);
    EXPECT_NEAR(CubicEaseInOutInverse(0.0625), 0.25, floatingPointError);
    EXPECT_NEAR(CubicEaseInOutInverse(.996), 0.9, floatingPointError);
}


TEST(CubicEaseInOutInverse, testAgainstCubicEaseIn){
    testEaseFuncAndInverse(&CubicEaseInOut, &CubicEaseInOutInverse);
}



