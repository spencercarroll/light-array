//
//  LightArrayUtilsTests.cpp
//  LightArray
//
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "LightArrayUtilsTests.h"
#include "gtest/gtest.h"
#include "LightArrayUtils.h"
#include "smarray.h"

static const double floatingPointError = 0.0000001;


TEST(rasterize, normal1){
    
    smarray<LALayer*> layers(3);
    smarray<LAFloat> result(3);

    layers[0] = new LALayer(0, *(new smarray<LAPixel>(3)));
    layers[1] = new LALayer(0, *(new smarray<LAPixel>(3)));
    layers[2] = new LALayer(1, *(new smarray<LAPixel>(2)));

    layers[2]->data[0].opacity = 1.0;
    layers[2]->data[0].brightness = 1.0;
    layers[2]->data[1].opacity = 1.0;
    layers[2]->data[1].brightness = 0.5;

    layers[1]->data[0].opacity = 0.5;
    layers[1]->data[0].brightness = 1.0;
    layers[1]->data[1].opacity = 0.5;
    layers[1]->data[1].brightness = 0.8;
    layers[1]->data[2].opacity = 0.5;
    layers[1]->data[2].brightness = 1.0;
    
    
    layers[0]->data[0].opacity = 1.0;
    layers[0]->data[0].brightness = 0.67;
    layers[0]->data[1].opacity = 0.1;
    layers[0]->data[1].brightness = .2;
    layers[0]->data[2].opacity = 0.1;
    layers[0]->data[2].brightness = .2;
    
    rasterize(layers, result);
    
    EXPECT_NEAR(result[0], 0.67, floatingPointError);
    EXPECT_NEAR(result[1], 0.83, floatingPointError);
    EXPECT_NEAR(result[2], 0.695, floatingPointError);
    
    delete &layers[0]->data;
    delete &layers[1]->data;
    delete &layers[2]->data;
    
    delete layers[0];
    delete layers[1];
    delete layers[2];
}



TEST(rasterize, offscreenData){
    
    smarray<LALayer*> layers(2);
    smarray<LAFloat> result(3);
    layers[0] = new LALayer(0, *(new smarray<LAPixel>(3)));
    layers[1] = new LALayer(2, *(new smarray<LAPixel>(3)));
    
    
    
    layers[1]->data[0].opacity = 0.5;
    layers[1]->data[0].brightness = 0.1;
    layers[1]->data[1].opacity = 0.5;
    layers[1]->data[1].brightness = 0.1;
    layers[1]->data[2].opacity = 0.5;
    layers[1]->data[2].brightness = 0.1;
    
    
    layers[0]->data[0].opacity = 0.5;
    layers[0]->data[0].brightness = 0.4;
    layers[0]->data[1].opacity = 0.5;
    layers[0]->data[1].brightness = .4;
    layers[0]->data[2].opacity = 0.5;
    layers[0]->data[2].brightness = .4;
    
    
    rasterize(layers, result);
    
    EXPECT_NEAR(result[0], 0.2, floatingPointError);
    EXPECT_NEAR(result[1], 0.2, floatingPointError);
    EXPECT_NEAR(result[2], 0.225, floatingPointError);

    delete &layers[0]->data;
    delete &layers[1]->data;
    
    delete layers[0];
    delete layers[1];
}

template <typename T>
void compareSmarray(smarray<T> &smarr, T primArr[]){
    for (size_t i = 0; i < smarr.length; i++) {
        EXPECT_NEAR(smarr[i], primArr[i], floatingPointError);
    }
}


TEST(generateGlowPattern, normal_positive){
    
    smarray<LAFloat> drawLayer(3);
    smarray<LARange<LAFloat> > glowPattern(3);
    glowPattern[0] = LARange<LAFloat>(0.5, 0.0);
    glowPattern[1] = LARange<LAFloat>(1.0, 0.5);
    glowPattern[2] = LARange<LAFloat>(0.0, 1.0);
    
    drawLayer.fill(0.0);
    int firstLoc = generateGlowPattern(4, 0, glowPattern, 2, 3, drawLayer);
    LAFloat expected0[3] = {0.5, 1.0, 0.0};
    compareSmarray(drawLayer, expected0);
    EXPECT_EQ(firstLoc, 2);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 1, glowPattern, 2, 3, drawLayer);
    LAFloat expected1[3] = {0.375, 0.875, 0.25};
    compareSmarray(drawLayer, expected1);
    EXPECT_EQ(firstLoc, 2);

    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 2, glowPattern, 2, 3, drawLayer);
    LAFloat expected2[3] = {0.25, 0.75, 0.50};
    compareSmarray(drawLayer, expected2);
    EXPECT_EQ(firstLoc, 2);

    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 3, glowPattern, 2, 3, drawLayer);
    LAFloat expected3[3] = {0.125, 0.625, 0.75};
    compareSmarray(drawLayer, expected3);
    EXPECT_EQ(firstLoc, 2);

    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 4, glowPattern, 2, 3, drawLayer);
    LAFloat expected4[3] = {0.0, 0.50, 1.0};
    compareSmarray(drawLayer, expected4);
    EXPECT_EQ(firstLoc, 3);

}


TEST(generateGlowPattern, normal_negative){
    
    smarray<LAFloat> drawLayer(3);
    smarray<LARange<LAFloat> > glowPattern(3);
    glowPattern[0] = LARange<LAFloat>(0.5, 0.0);
    glowPattern[1] = LARange<LAFloat>(1.0, 0.5);
    glowPattern[2] = LARange<LAFloat>(0.0, 1.0);
    
    
    LAFloat start = 0;
    LAFloat end = -1;
    drawLayer.fill(0.0);
    int firstLoc = generateGlowPattern(4, 0, glowPattern, start, end, drawLayer);
    LAFloat expected0[3] = {0.0, 1.0, 0.5};
    compareSmarray(drawLayer, expected0);
    EXPECT_EQ(firstLoc, start);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 1, glowPattern, start, end, drawLayer);
    LAFloat expected1[3] = {0.25, 0.875, 0.375};
    compareSmarray(drawLayer, expected1);
    EXPECT_EQ(firstLoc, start);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 2, glowPattern, start, end, drawLayer);
    LAFloat expected2[3] = {0.50, 0.75, 0.25};
    compareSmarray(drawLayer, expected2);
    EXPECT_EQ(firstLoc, start);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 3, glowPattern, start, end, drawLayer);
    LAFloat expected3[3] = {0.75, 0.625, 0.125};
    compareSmarray(drawLayer, expected3);
    EXPECT_EQ(firstLoc, start);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 4, glowPattern, start, end, drawLayer);
    LAFloat expected4[3] = {1.0, 0.50, 0.0};
    compareSmarray(drawLayer, expected4);
    EXPECT_EQ(firstLoc, end);
}

TEST(generateGlowPattern, startAndEndOffScreen_Pos){
    
    smarray<LAFloat> drawLayer(3);
    smarray<LARange<LAFloat> > glowPattern(3);
    glowPattern[0] = LARange<LAFloat>(0.5, 0.0);
    glowPattern[1] = LARange<LAFloat>(1.0, 0.5);
    glowPattern[2] = LARange<LAFloat>(0.0, 1.0);
    
    
    LAFloat start = -1;
    LAFloat end = 4;
    drawLayer.fill(0.0);
    int firstLoc = generateGlowPattern(4, 0, glowPattern, start, end, drawLayer);
    LAFloat expected0[3] = {0.0, 0.0, 0.0};
    compareSmarray(drawLayer, expected0);
    EXPECT_EQ(firstLoc, start);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 1, glowPattern, start, end, drawLayer);
    LAFloat expected1[3] = {0.25, 0.0, 0.0};
    compareSmarray(drawLayer, expected1);
    EXPECT_EQ(firstLoc, 0);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 2, glowPattern, start, end, drawLayer);
    LAFloat expected2[3] = {0.75, 0.5, 0.0};
    compareSmarray(drawLayer, expected2);
    EXPECT_EQ(firstLoc, 1);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 3, glowPattern, start, end, drawLayer);
    LAFloat expected3[3] = {0.125, 0.625, 0.75};
    compareSmarray(drawLayer, expected3);
    EXPECT_EQ(firstLoc, 2);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 4, glowPattern, start, end, drawLayer);
    LAFloat expected4[3] = {0.0, 0.0, 0.50};
    compareSmarray(drawLayer, expected4);
    EXPECT_EQ(firstLoc, 4);
}

TEST(generateGlowPattern, startAndEndOffScreen_Neg){
    
    smarray<LAFloat> drawLayer(3);
    smarray<LARange<LAFloat> > glowPattern(3);
    glowPattern[0] = LARange<LAFloat>(0.5, 0.0);
    glowPattern[1] = LARange<LAFloat>(1.0, 0.5);
    glowPattern[2] = LARange<LAFloat>(0.0, 1.0);
    
    
    LAFloat start = 3;
    LAFloat end = -2;
    drawLayer.fill(0.0);
    int firstLoc = generateGlowPattern(4, 0, glowPattern, start, end, drawLayer);
    LAFloat expected0[3] = {0.0, 0.0, 0.0};
    compareSmarray(drawLayer, expected0);
    EXPECT_EQ(firstLoc, start);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 1, glowPattern, start, end, drawLayer);
    LAFloat expected1[3] = {0.0, 0.0, 0.25};
    compareSmarray(drawLayer, expected1);
    EXPECT_EQ(firstLoc, 2);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 2, glowPattern, start, end, drawLayer);
    LAFloat expected2[3] = {0.0, 0.5, 0.75};
    compareSmarray(drawLayer, expected2);
    EXPECT_EQ(firstLoc, 1);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 3, glowPattern, start, end, drawLayer);
    LAFloat expected3[3] = {0.75, 0.625, 0.125};
    compareSmarray(drawLayer, expected3);
    EXPECT_EQ(firstLoc, 0);
    
    drawLayer.fill(0.0);
    firstLoc = generateGlowPattern(4, 4, glowPattern, start, end, drawLayer);
    LAFloat expected4[3] = {0.5, 0.0, 0.0};
    compareSmarray(drawLayer, expected4);
    EXPECT_EQ(firstLoc, -2);
}









