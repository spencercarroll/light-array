//
//  TestPlatform.cpp
//  LightArray
//
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "TestPlatform.h"




TestPlatform::TestPlatform(int numChannels) : TestPlatform(numChannels, false){
    
}

TestPlatform::TestPlatform(int numChannels, bool printUpdates) :
cNumChannels(numChannels),
channels(new LAFloat[numChannels]),
printUpdates(printUpdates){
    
}


TestPlatform::~TestPlatform(){
    delete channels;
}

void TestPlatform::setChannel(int channel, LAFloat value){
    channels[channel] = value;
}

LAFloat TestPlatform::getChannel(int channel){
    return channels[channel];
}
void TestPlatform::clearChannel(int channel){
    channels[channel] = 0;
}
void TestPlatform::clearAll(){
    for (int i = 0; i<numChannels(); i++) {
        channels[i] = 0;
    }
}

void TestPlatform::update(){
    
    if (!printUpdates) {
        return;
    }
    
    for (int i = 0; i<numChannels(); i++) {
        std::cout << channels[i] << "       ";
    }
    std::cout << "\n";
}

void TestPlatform::pause(unsigned long millis){
    //do nothing
}
const int TestPlatform::numChannels(){
    return cNumChannels;
}