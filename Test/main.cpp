//
//  main.cpp
//  LightArray
//
//  Created by Spencer Carroll on 9/8/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//


#include "gtest/gtest.h"

GTEST_API_ int main(int argc, char **argv) {
    printf("Running main() from gtest_main.cc\n");
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
