//
//  smarrayTests.cpp
//  LightArray
//
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "smarrayTests.h"
#include "gtest/gtest.h"
#include "smarray.h"

TEST(reverse, odd){
    
    int y[3] = {0,1,2};
    int yRev[3] = {2,1,0};
    
    smarray<int> b(y, 3);
    b.reverse(0, b.length-1);
    
    for (int i = 0; i < b.length; i++) {
        EXPECT_EQ(b[i], yRev[i]);
    }
}

TEST(reverse, even){
    int x[4] = {0,1,2,3};
    int xRev[4] = {3,2,1,0};
    
    smarray<int> a(x, 4);
    a.reverse(0, a.length-1);
    
    for (int i = 0; i < a.length; i++) {
        EXPECT_EQ(a[i], xRev[i]);
    }
    
}

TEST(reverse, subsectionOdd){
    
    
    int y[3] = {0,1,2};
    int yRev[3] = {1,0,2};
    
    smarray<int> b(y, 3);
    b.reverse(0, 1);
    
    for (int i = 0; i < b.length; i++) {
        EXPECT_EQ(b[i], yRev[i]);
    }
    
}

TEST(reverse, subsectionEven){

    int x[4] = {0,1,2,3};
    int xRev[4] = {0,3,2,1};
    
    smarray<int> a(x, 4);
    a.reverse(1, 3);
    
    for (int i = 0; i < a.length; i++) {
        EXPECT_EQ(a[i], xRev[i]);
    }
}




