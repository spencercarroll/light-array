//
//  TestPlatform.h
//  LightArray
//
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__TestPlatform__
#define __LightArray__TestPlatform__


#include <iostream>
#include "Platform.h"

#include "LAConstants.h"

class TestPlatform : public Platform{
    
private:
    const int cNumChannels;
    LAFloat* const channels;
    
public:
    bool printUpdates;
    
    TestPlatform(int numChannels);
    TestPlatform(int numChannels, bool printUpdates);
    ~TestPlatform();
    virtual void setChannel(int channel, LAFloat value);
    virtual LAFloat getChannel(int channel);
    virtual void clearChannel(int channel);
    virtual void clearAll();
    virtual void update();
    virtual void pause(unsigned long millis);
    virtual const int numChannels();
};
#endif /* defined(__LightArray__TestPlatform__) */
