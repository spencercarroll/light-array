//
//  ShowDescription.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//


#ifndef SHOW_DESCRIPTION_H
#define SHOW_DESCRIPTION_H

#include "Show.h"
#include "LAConstants.h"
class ShowDescription {
    

public:
    /* MirrorFactor - proportion of times the show should be mirrored.
     Ex:0.0 indicates the show should never mirrored.
     1.0 indicates it should always be mirrored
     0.7 indicates it should be mirrored 70% of the time.
     */
    const LAFloat cMirrorFactor;
    //  The speed the show should be played at. Ex: 1.0 would be normal. 0.0 would be infinitely long(and a div/0 error). 2.0 would be twice as fast.
    const LAFloat cSpeed;
    Show * const cShow;
    
    ShowDescription(Show* const show, const LAFloat mirrorFactor, const LAFloat speed)
    : cShow(show), cMirrorFactor(mirrorFactor), cSpeed(speed){
    
    };
};


#endif