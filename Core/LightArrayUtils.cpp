//
//  LightArrayUtils.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/7/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "LightArrayUtils.h"
#include <stdlib.h>






float randomFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

static bool isInRange(ptrdiff_t i, ptrdiff_t lower, ptrdiff_t upper){
    return i>=lower && i<=upper;
}

int generateGlowPatternPositive(int totalSteps, int curStep, smarray<LARange<LAFloat> > &glowPattern, LAFloat start, LAFloat end, smarray<LAFloat> &results){
    LAFloat fractionComplete = curStep/(LAFloat)totalSteps;
    
    LAFloat curPoint = start + (end - start)*fractionComplete;
    long glowEnd = (int)curPoint;
    if (curPoint < 0) {
        glowEnd = (LAFloat)((long)curPoint) == curPoint ? (long)curPoint : (long)curPoint - 1;
    }
    long glowStart = glowEnd - glowPattern.length + 1;
    LAFloat glowFraction = curPoint < 0 ? 1+(curPoint - (int)curPoint) : curPoint - (int)curPoint;
    size_t glowPatternIndex = 0;
    for (long i = glowStart; i <= glowEnd; i++) {
        LARange<LAFloat> posRange = glowPattern[glowPatternIndex++];
        LAFloat v0 = posRange.start + (posRange.end - posRange.start)*glowFraction;
        if (isInRange((int)i, 0, (int)results.length-1)) {
            results[i] = v0;
        }
    }
    return (int)glowEnd;
}


int generateGlowPatternNegative(int totalSteps, int curStep, smarray<LARange<LAFloat> > &glowPattern, LAFloat start, LAFloat end, smarray<LAFloat> &results){
    
    LAFloat revStart = results.length - 1 - start;
    LAFloat revEnd = results.length - 1 - end;
    int revFirstLoc = generateGlowPattern(totalSteps, curStep, glowPattern, revStart, revEnd, results);
    results.reverse(0, results.length-1);
    return (int)results.length - 1 - revFirstLoc;
}
int generateGlowPattern(int totalSteps, int curStep, smarray<LARange<LAFloat> > &glowPattern, LAFloat start, LAFloat end, smarray<LAFloat> &results){
    
    if (start > end){
        return generateGlowPatternNegative(totalSteps, curStep, glowPattern, start, end, results);
    }else{
        return generateGlowPatternPositive(totalSteps, curStep, glowPattern, start, end, results);
    }
}


void rasterize(smarray<LALayer *> &layers, smarray<LAFloat> &result){
    
    result.fill(0.0);
    for (ptrdiff_t i = layers.length-1; i >= 0; i--) {
        LALayer &curLayer = *layers[i];
        size_t layerIndex = 0;
        for (ptrdiff_t resultIndex = curLayer.offset; resultIndex < (curLayer.data.length + curLayer.offset); resultIndex++) {
            LAPixel &curPixel = curLayer.data[layerIndex++];
            LAFloat newValue = curPixel.brightness * curPixel.opacity + result[resultIndex]*(1-curPixel.opacity);
            if (isInRange(resultIndex, 0, result.length-1)) {
                result[resultIndex] = newValue;
            }
        }
    }
    
    
    
}

