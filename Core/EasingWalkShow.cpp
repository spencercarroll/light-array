//
//  EasingWalk.cpp
//  TestEmbedXcode
//
//  Created by Spencer Carroll on 8/9/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "EasingWalkShow.h"
#include "easing.h"

static const int glowDelay = 12;
static const int easeFuncSteps = 50;
static const int easingWalkMinRequiredChannels = 4;

EasingWalkShow::EasingWalkShow(Device &dev) : Show(dev, easingWalkMinRequiredChannels){
    
}

void EasingWalkShow::play(){

    AHFloat (*easeFunc)(AHFloat) = &CubicEaseInOut;
    int numChannels = dev.numChannels();
    
    
    //set 0th channel to high
    for (int j = 0; j <= easeFuncSteps; j++) {
        AHFloat brightnessVal = easingValue(easeFuncSteps, j, 0.0, 1.0, easeFunc);
        dev.setUpdatePause(0, brightnessVal, glowDelay);
    }
    
    for(int i =0; i<numChannels; i++){
        if( i == numChannels-1){
            for (int j = 0; j <= easeFuncSteps; j++) {
                AHFloat brightnessVal = easingValue(easeFuncSteps, j, 1.0, 0.0, easeFunc);
                dev.setUpdatePause(i, brightnessVal, glowDelay);
            }
            continue;
        }
        
        int down = i;
        int up = i+1;
        for (int j = 0; j <= easeFuncSteps; j++) {
            AHFloat downBrightnessVal = easingValue(easeFuncSteps, j, 1.0, 0.0, easeFunc);
            AHFloat upBrightnessVal = easingValue(easeFuncSteps, j, 0.0, 1.0, easeFunc);
            dev.setChannel(down, downBrightnessVal);
            dev.setUpdatePause(up, upBrightnessVal, glowDelay);
        }
    }
}


