
#include "WizardShow.h"

static const LAFloat minSpellBrightness = 0.1;
static const float movementOnSpellWinFactor = 0.1; //moves numChannels*movementOnSpellWinFactor on a win in win direction.
static const int stepsPerRound = 110;
static const LAFloat wizardBrightness = 0.9;
static const size_t wizardOffsetFromEnd = 1;
static const unsigned long spellMovementDelay = 5;
static const unsigned long roundDelay = 200;
static const unsigned long finishedDelay = 1000;
static const int wizardShowMinRequiredChannels = 10;

WizardShow::WizardShow(Device &dev) : Show(dev, wizardShowMinRequiredChannels){

}



void WizardShow::play(){
    
    const int numChannels = dev.numChannels();
    const int movementOnSpellWin = (int)(movementOnSpellWinFactor * numChannels);
    
    smarray<LAFloat> drawLayer(numChannels);
    smarray<LALayer *> toBeRasteredLayers(4);
    
    smarray<LARange<LAFloat> > glowPattern(3);
    glowPattern[0] = LARange<LAFloat>(0.5, 0.0);
    glowPattern[1] = LARange<LAFloat>(1.0, 0.5);
    glowPattern[2] = LARange<LAFloat>(0.0, 1.0);
    
    const int rightWizardLoc = numChannels-wizardOffsetFromEnd-1;
    const int leftWizardLoc = wizardOffsetFromEnd;
    
    smarray<LAPixel> leftWizardLayerData(1);
    smarray<LAPixel> rightWizardLayerData(1);
    smarray<LAPixel> leftSpellLayerData(numChannels);
    smarray<LAPixel> rightSpellLayerData(numChannels);
    
    LALayer leftWizardLayer(1, leftWizardLayerData);
    LALayer rightWizardLayer(rightWizardLoc, rightWizardLayerData);
    LALayer leftSpellLayer(0, leftSpellLayerData);
    LALayer rightSpellLayer(0, rightSpellLayerData);
    
    leftWizardLayer.data[0] = LAPixel(wizardBrightness, 1.0);
    rightWizardLayer.data[0] = LAPixel(wizardBrightness, 1.0);
    
    int equilibriumPoint = (numChannels-1)/2;//not necessarily fair.
    
    while (true) {//each loop is a round.
        dev.pause(roundDelay);
        
        LAFloat leftSpellBrightness = randomSpell();
        LAFloat rightSpellBrightness = randomSpell();
        
        int leftSpellFinalLoc = equilibriumPoint + movementOnSpellWin;
        int rightSpellFinalLoc = equilibriumPoint - movementOnSpellWin;
        equilibriumPoint = leftSpellBrightness > rightSpellBrightness ? equilibriumPoint + movementOnSpellWin : equilibriumPoint - movementOnSpellWin;
        
        
        
        LALayer *winWizardLayer = NULL;
        LALayer *loseWizardLayer = NULL;
        LALayer *winSpellLayer = NULL;
        LALayer *loseSpellLayer = NULL;
        LAFloat winStart = 0.0;
        LAFloat winEnd = 0.0;
        bool winHasOccured = false;
        if (equilibriumPoint>=rightWizardLoc) {//left wizard has won.
            winHasOccured = true;
            winWizardLayer = &leftWizardLayer;
            loseWizardLayer = &rightWizardLayer;
            winSpellLayer = &leftSpellLayer;
            loseSpellLayer = &rightSpellLayer;
            winStart = leftWizardLoc;
            winEnd = numChannels+glowPattern.length-1;
            
        }
        if (equilibriumPoint<=leftWizardLoc) {//right wizard has won.
            winHasOccured = true;
            winWizardLayer = &rightWizardLayer;
            loseWizardLayer = &leftWizardLayer;
            winSpellLayer = &rightSpellLayer;
            loseSpellLayer = &leftSpellLayer;
            winStart = rightWizardLoc;
            winEnd =  -1*((int)glowPattern.length);//eeek those darn size_ts can accidentally make this positive.
        }
        
        if (winHasOccured) {
            
            
            for (size_t i = 0; i < loseSpellLayer->data.length; i++) {
                loseSpellLayer->data[i].brightness = 0.0;
            }
            
            toBeRasteredLayers[0] = winWizardLayer;
            toBeRasteredLayers[1] = winSpellLayer;
            toBeRasteredLayers[2] = loseWizardLayer;
            toBeRasteredLayers[3] = loseSpellLayer;
            
            
            for (int curStep = 0; curStep <= stepsPerRound; curStep++) {
                drawLayer.fill(0.0);
                int glowEnd = generateGlowPattern(stepsPerRound, curStep, glowPattern, winStart, winEnd, drawLayer);
                for (int i = 0; i < drawLayer.length; i++) {
                    winSpellLayer->data[i].brightness = drawLayer[i];//don't dim by multiplying with win spell brightness
                    winSpellLayer->data[i].opacity = 1.0;
                }
                
                
                //make some of the winning spell transparent
                if (equilibriumPoint<=leftWizardLoc){//right won
                    for (ptrdiff_t i = 0; i < glowEnd; i++) {
                        winSpellLayer->data[i].opacity = 0.0;
                    }
                }else{//left won
                    for (ptrdiff_t i = numChannels - 1; i > glowEnd; i--) {
                        winSpellLayer->data[i].opacity = 0.0;
                    }
                }
                drawLayer.fill(0.0);
                rasterize(toBeRasteredLayers, drawLayer);
                for (int i = 0; i < drawLayer.length; i++) {
                    dev.setChannel(i, drawLayer[i]);
                }
                dev.update();
                dev.pause(spellMovementDelay);
            }
            dev.pause(finishedDelay);
            return;
        }
        
        
        //no winner. show duel
        for (int curStep = 0; curStep <= stepsPerRound; curStep++) {
            drawLayer.fill(0.0);
            int leftGlowEnd = generateGlowPattern(stepsPerRound, curStep, glowPattern, leftWizardLoc, leftSpellFinalLoc, drawLayer);
            for (size_t i = 0; i < drawLayer.length; i++) {
                leftSpellLayer.data[i].brightness = drawLayer[i] * leftSpellBrightness;
                leftSpellLayer.data[i].opacity = 1.0;
            }
            
            
            drawLayer.fill(0.0);
            int rightGlowEnd = generateGlowPattern(stepsPerRound, curStep, glowPattern, rightWizardLoc, rightSpellFinalLoc, drawLayer);
            for (size_t i = 0; i < drawLayer.length; i++) {
                rightSpellLayer.data[i].brightness = drawLayer[i] * rightSpellBrightness;
                rightSpellLayer.data[i].opacity = 1.0;
            }
            
            
            toBeRasteredLayers[0] = &rightWizardLayer;
            toBeRasteredLayers[1] = &leftWizardLayer;
            
            //the winning spell is positioned above the losing spell but it's opacity needs to be 0.0 past the winning spells end.
            if (leftSpellBrightness > rightSpellBrightness) {
                for (ptrdiff_t i = numChannels - 1; i > leftGlowEnd; i--) {
                    leftSpellLayer.data[i].opacity = 0.0;
                }
                toBeRasteredLayers[2] = &leftSpellLayer;
                toBeRasteredLayers[3] = &rightSpellLayer;
            }else{
                for (ptrdiff_t i = 0; i < rightGlowEnd; i++) {
                    rightSpellLayer.data[i].opacity = 0.0;
                }
                toBeRasteredLayers[2] = &rightSpellLayer;
                toBeRasteredLayers[3] = &leftSpellLayer;
            }
            
            
            drawLayer.fill(0.0);
            rasterize(toBeRasteredLayers, drawLayer);
            for (int i = 0; i < drawLayer.length; i++) {
                dev.setChannel(i, drawLayer[i]);
            }
            dev.pause(spellMovementDelay);
            dev.update();
        }
        
    }
    
    
}



LAFloat WizardShow::randomSpell(){
    LAFloat rand = randomFloat(0.0, 1.0-minSpellBrightness) + minSpellBrightness; 
    return rand;
}
