//
//  LightArrayUtils.h
//  LightArray
//
//  Created by Spencer Carroll on 8/7/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef LightArray_ShowUtils_h
#define LightArray_ShowUtils_h

#include "LAConstants.h"
#include "smarray.h"
#include <stddef.h>


template < class T >
class LARange {
public:
    LARange(T start, T end): start(start), end(end){};
    T start;
    T end;
};


class LAPixel {
public:
    LAPixel(LAFloat brightness, LAFloat opacity): brightness(brightness), opacity(opacity){}
    LAFloat brightness;
    LAFloat opacity;
};

class LALayer {
public:
    LALayer(ptrdiff_t offset, smarray<LAPixel> & data): offset(offset), data(data){}
    ptrdiff_t offset;
    smarray<LAPixel> &data;
};


//This is based on the stdlib random function. Seed it appropriately before use.
float randomFloat(float a, float b);//value in range [a,b]

//Processed from last to first, ie layers[0] cannot be overwritten by layers below it.
void rasterize(smarray<LALayer*> &layers, smarray<LAFloat> &result);

//returns the first channel on moving in the "forward" direction.
int generateGlowPattern(int totalSteps, int curStep, smarray<LARange<LAFloat> > &glowPattern, LAFloat start, LAFloat end, smarray<LAFloat> &results);






#endif
