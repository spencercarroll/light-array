//
//  ShowRunner.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//


#ifndef SHOW_RUNNER_H
#define SHOW_RUNNER_H

#include "smarray.h"
#include "ShowDescription.h"

class ShowRunner{

private:
    smarray<ShowDescription const *> const & showDescriptions;
    Device &dev;
    void runShow(ShowDescription const * description);
public:
    void run();
    ShowRunner(smarray<ShowDescription const *> &showDescriptions, Device &dev);
    
};



#endif