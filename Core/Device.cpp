//
//  File.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "Device.h"
#include <math.h>

Device :: Device(Platform *platform): cPlatform(platform), mIsMirrored(false), mSpeed(1.0){
    
}

Device :: ~Device(){

}

const int  Device::mirrorCorrectedChannel(int channel){
    return mIsMirrored ? ((cPlatform->numChannels() - 1) - channel) : channel;
}


void Device::setChannel(int channel, LAFloat value){
    cPlatform->setChannel(mirrorCorrectedChannel(channel), value);
}

void Device::setUpdatePause(int channel, LAFloat value, unsigned long millis){
    cPlatform->setChannel(channel, value);
    cPlatform->update();
    cPlatform->pause(millis);
}


LAFloat Device::getChannel(int channel){
    return cPlatform->getChannel(mirrorCorrectedChannel(channel));
}

void Device::clearChannel(int channel){
    cPlatform->clearChannel(mirrorCorrectedChannel(channel));
}


void  Device::clearAll(){
    cPlatform->clearAll();
}

void  Device::pause(unsigned long millis){
    cPlatform->pause(millis * mSpeed);
}

void Device::update(){
    cPlatform->update();
}

const int Device::numChannels(){
    return cPlatform->numChannels();
}

bool Device::isMirrored(){
    return mIsMirrored;
}

void Device::setMirrored(bool shouldBeMirrored){
    mIsMirrored = shouldBeMirrored;
}

LAFloat Device::getSpeed(){
    return mSpeed;
}

void Device::setSpeed(LAFloat speed){
    mSpeed = fabs(speed);

}

