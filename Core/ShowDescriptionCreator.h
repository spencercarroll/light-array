//
//  ShowDescriptionCreator.h
//  LightArray
//
//  Created by Spencer Carroll on 8/8/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__ShowDescriptionCreator__
#define __LightArray__ShowDescriptionCreator__

#include "ShowDescription.h"
#include "Device.h"
#include "smarray.h"


class ShowDescriptionCreator {
    
public:
    smarray<ShowDescription const *>* createShowDescriptions(Device &dev);
};

#endif /* defined(__LightArray__ShowDescriptionCreator__) */
