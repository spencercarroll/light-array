//
//  MergeSortShow.h
//  LightArray
//
//  Created by Spencer Carroll on 8/10/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__MergeSortShow__
#define __LightArray__MergeSortShow__

#include "Show.h"
#include "smarray.h"
#include "LAConstants.h"


class MergeSortShow : public Show{
    
private:
    void initialize();
    void mergeSort(size_t lo, size_t hi, smarray<LAFloat> &h);
    void merge(size_t i1, size_t i2, size_t end, smarray<LAFloat> &h);
public:
    virtual void play();
    MergeSortShow(Device &dev);
};


#endif /* defined(__LightArray__MergeSortShow__) */
