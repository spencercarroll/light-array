//
//  SpiralAndGlowShow.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__SpiralAndGlowShow__
#define __LightArray__SpiralAndGlowShow__

#include "Show.h"
#include "Device.h"



class SpiralAndGlowShow : public Show{
    
    
public:
    virtual void play();
    SpiralAndGlowShow(Device &dev);
    
};
#endif /* defined(__LightArray__SpiralAndGlowShow__) */
