//
//  BubbleSortShow.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__BubbleSortShow__
#define __LightArray__BubbleSortShow__

#include "Show.h"

class BubbleSortShow : public Show{
    
private:
    void initialize();
    
public:
    virtual void play();
    BubbleSortShow(Device &dev);
};


#endif /* defined(__LightArray__BubbleSortShow__) */
