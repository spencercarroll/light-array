//
//  BubbleSortShow.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "BubbleSortShow.h"
#include "LightArrayUtils.h"
static const unsigned long intialDelay = 1000;
static const unsigned long innerDelay = 60;
static const unsigned long outerDelay = 275;
static const unsigned long finishedDelay = 2000;
static const int bubbleSortMinRequiredChannels = 4;



BubbleSortShow::BubbleSortShow(Device &dev) : Show(dev, bubbleSortMinRequiredChannels){

}


void BubbleSortShow::initialize(){
    
    for (int i = 0; i < dev.numChannels(); i++) {
        dev.setChannel(i, randomFloat(0.0, 1.0));
    }

    dev.update();
    dev.pause(intialDelay);
}



void BubbleSortShow::play(){
    initialize();
    //lastIndex is inclusive
    for(int lastIndex = dev.numChannels()-1; lastIndex > 0; lastIndex--){

        for (int j = 0; j < lastIndex; j++) {
            int i1 = j;
            int i2 = j+1;
            LAFloat v1 = dev.getChannel(i1);
            LAFloat v2 = dev.getChannel(i2);

            
            if (v1 > v2) {
                dev.setChannel(i1, v2);
                dev.setChannel(i2, v1);
            }
            dev.update();
            dev.pause(innerDelay);
        }
        dev.pause(outerDelay);
    }
    dev.pause(finishedDelay);
}


