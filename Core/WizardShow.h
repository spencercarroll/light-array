//
//  WizardShow.h
//  LightArray
//
//  Created by Spencer Carroll on 8/10/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__WizardShow__
#define __LightArray__WizardShow__


#include "Show.h"
#include "LAConstants.h"
#include "LightArrayUtils.h"
#include "smarray.h"


class WizardShow : public Show{
    
private:
   LAFloat randomSpell();
    
public:
    virtual void play();
    WizardShow(Device &dev);
};

#endif /* defined(__LightArray__WizardShow__) */
