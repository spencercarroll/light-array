//
//  ShowRunner.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "ShowRunner.h"
#include "Device.h"
#include "LightArrayUtils.h"

static const unsigned long millisBetweenShows = 1000;

ShowRunner::ShowRunner(smarray<ShowDescription const *> &showDescriptions, Device &dev) :
showDescriptions(showDescriptions), dev(dev){
}



void ShowRunner::run(){
    
    ShowDescription const * previousDescription = NULL;
    while (true) {
        if(showDescriptions.length==1){
            runShow(showDescriptions.data[0]);
        }
        if (showDescriptions.length==2) {
            runShow(showDescriptions.data[0]);
            runShow(showDescriptions.data[1]);
        }
        if(showDescriptions.length>2){
            ShowDescription const *curDescription;
            //Don't run any show twice in a row
            while (true) {
                //Oh boy! Its possible that the random function could return 1.0
                size_t index = ((size_t)(randomFloat(0.0,1.0)*showDescriptions.length)) % ((size_t)showDescriptions.length);
                curDescription = showDescriptions.data[index];
                if (curDescription != previousDescription) {
                    break;
                }
            }
            runShow(curDescription);
            previousDescription = curDescription;
        }
    }
}

void ShowRunner::runShow(ShowDescription const * description){
    

    
    if(description->cShow->minRequiredChannels > dev.numChannels()){
        return;
    }
    dev.setSpeed(description->cSpeed);
    bool shouldBeMirrored = (randomFloat(0.0,1.0) > description->cMirrorFactor) ? false : true;
    dev.setMirrored(shouldBeMirrored);
    description->cShow->play();
    dev.setSpeed(1.0);
    dev.clearAll();
    dev.pause(millisBetweenShows);
}
