//
//  MergeSortShow.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/10/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "MergeSortShow.h"
#include "LightArrayUtils.h"

static const unsigned long initialDelay = 1000;
static const unsigned long insertDelay = 70;
static const unsigned long mergeFinishedDelay = 1000;
static const unsigned long finalMergeDelay = 4000;
static const int mergeSortMinRequiredChannels = 5;


MergeSortShow::MergeSortShow(Device &dev) : Show(dev, mergeSortMinRequiredChannels){
    
}


void MergeSortShow::initialize(){
    
    for (int i = 0; i < dev.numChannels(); i++) {
        dev.setChannel(i, randomFloat(0.0, 1.0));
    }
    
    dev.update();
    dev.pause(initialDelay);
}

void MergeSortShow::play(){
    initialize();
    smarray<LAFloat> h(dev.numChannels());
    mergeSort(0, dev.numChannels()-1, h);
}

void MergeSortShow::mergeSort(size_t lo, size_t hi, smarray<LAFloat> &h){
    if(lo>=hi)
        return;
    
    size_t mid = lo + (hi-lo)/2;
    mergeSort(lo, mid, h);
    mergeSort(mid+1, hi, h);
    
    
    if ((hi - lo) > 1) {
        if (lo==0 && hi == dev.numChannels()-1) {
            dev.pause(finalMergeDelay);
        }else{
            dev.pause(mergeFinishedDelay);
        }
    }
    
    
    merge(lo, mid+1, hi, h);

}

//All indices are inclusive
void MergeSortShow::merge(size_t i1, size_t i2, size_t end, smarray<LAFloat> &h){
    
    
    for (size_t j = i1; j <= end; j++) {
        h[j] = dev.getChannel((int)j);
    }
    
    size_t curI1 = i1;
    size_t curI2 = i2;
    size_t curMergeIndex = i1;
    
    while (curMergeIndex <= end) {
        
        if (curI1 >= i2) {
            break;
        }
        
        if(curI2>end){
            while (curMergeIndex <= end) {
                dev.setUpdatePause((int)curMergeIndex++, h[curI1++], insertDelay);
            }
            break;
        }
        
        if(h[curI1] > h[curI2]){
            dev.setUpdatePause((int)curMergeIndex++, h[curI2++], insertDelay);
        }else{
            dev.setUpdatePause((int)curMergeIndex++, h[curI1++], insertDelay);
        }
        
    }
}


