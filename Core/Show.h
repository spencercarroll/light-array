//
//  Show.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef SHOW_H
#define SHOW_H

#include "Device.h"

class Show {
        
public:
    Device &dev;
    virtual void play()=0;
    const int minRequiredChannels;
    Show(Device &dev, int minRequiredChannels) : dev(dev), minRequiredChannels(minRequiredChannels){}
};

#endif