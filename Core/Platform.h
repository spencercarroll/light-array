//
//  Platform.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//


#ifndef __LightArray__Platform__
#define __LightArray__Platform__

#include "LAConstants.h"

class Platform {
    
public:
    virtual void setChannel(int channel, LAFloat value) = 0;
    virtual LAFloat getChannel(int channel) = 0;
    virtual void clearChannel(int channel) = 0;
    virtual void clearAll() = 0;
    virtual void update() = 0;
    virtual void pause(unsigned long millis) = 0;
    virtual const int numChannels() = 0;

};


#endif