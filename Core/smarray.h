//
//  smarray.h
//  LightArray
//
//  Created by Spencer Carroll on 8/12/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__smarray__
#define __LightArray__smarray__

#include <stddef.h>
#include <stdlib.h>
/*
    smarray, "sm" "array", stands for simple array. It is a fixed size array.
    This array's data is always malloc'ed. If running on embedded systems consider memory fragmentation occuring.
 */

template <class T>
class smarray {
    
private:
    
public:
    const size_t length;
    T*  const data;
    smarray(size_t length):length(length), data((T*)malloc(length * sizeof(T))){
    };

    smarray(T arr[], size_t length): smarray(length){
        for(int i = 0; i < length; i++){
            data[i] = arr[i];
        }
    }
    
    ~smarray(){
        free(data);
    }
    

    T& operator[] (const size_t nIndex){
        return data[nIndex];
    }
    
    void fill(const T &val){
        for (size_t i = 0; i < length; i++) {
            data[i] = val;
        }
    }
    
    //inclusive start and end
    void reverse(size_t start, size_t end){
        size_t diff = end - start;
        size_t revMid = (diff + diff%2 ) / 2 + start;
        for (size_t i = start; i < revMid ; i++) {
            size_t opposite =  end - (i - start);
            T temp = data[i];
            data[i] = data[opposite];
            data[opposite] = temp;
        }
    }
};





#endif /* defined(__LightArray__smarray__) */
