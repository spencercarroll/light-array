//
//  EasingWalk.h
//  LightArray
//
//  Created by Spencer Carroll on 8/9/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArray__EasingWalk__
#define __LightArray__EasingWalk__

#include "Show.h"

class EasingWalkShow : public Show{
    
private:
    
public:
    virtual void play();
    EasingWalkShow(Device &dev);
};


#endif /* defined(__LightArray__EasingWalk__) */
