//
//  ShowDescriptionCreator.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/8/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "ShowDescriptionCreator.h"
#include "BubbleSortShow.h"
#include "SpiralAndGlowShow.h"
#include "EasingWalkShow.h"
#include "MergeSortShow.h"
#include "WizardShow.h"


smarray<ShowDescription const *>* ShowDescriptionCreator::createShowDescriptions(Device &dev){
    size_t len = 6;
    
    smarray<ShowDescription const *>* const descriptions = new smarray<ShowDescription const *>(len);
    descriptions->data[0] = new ShowDescription(new WizardShow(dev), 0.0, 1.0);
    descriptions->data[1] = new ShowDescription(new BubbleSortShow(dev), 0.0, 0.7);
    descriptions->data[2] = new ShowDescription(new EasingWalkShow(dev), 0.0, 0.68);
    descriptions->data[3] = new ShowDescription(new MergeSortShow(dev), 0.0, 1.1);
    descriptions->data[4] = new ShowDescription(new SpiralAndGlowShow(dev), 0.0, 1.0);
    descriptions->data[5] = new ShowDescription(new WizardShow(dev), 0.0, 1.0);

    return descriptions;
}

