//
//  SpiralAndGlowShow.cpp
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "SpiralAndGlowShow.h"
#include "easing.h"
#include "smarray.h"
#include "LightArrayUtils.h"

static const unsigned long initialDelay = 1000;
static const unsigned long movementDelay = 75;
static const unsigned long glowDelay = 15;
static const unsigned long finishedDelay = 2000;
static const int spiralAndGlowMinRequiredChannels = 5;



SpiralAndGlowShow::SpiralAndGlowShow(Device &dev) : Show(dev, spiralAndGlowMinRequiredChannels){
    
}

//both inclusive
static inline bool isInRange(ptrdiff_t i, ptrdiff_t lower, ptrdiff_t upper){
    return i>=lower && i<=upper;
}
static void addInRange(smarray<LAFloat> &base, smarray<LAFloat> &toAdd, ptrdiff_t offset) {
    
    for(ptrdiff_t i = 0; i < toAdd.length; i++){
        if (isInRange(offset+i, 0, (ptrdiff_t)base.length - 1)) {
            base[offset+i] =  base[offset+i] + toAdd.data[i];
        }
    }
}


void SpiralAndGlowShow::play(){
    
    
    int const numChannels = dev.numChannels();
    
    smarray<LAFloat> base(numChannels);
    
    LAFloat initialVal = 0;
    base.fill(initialVal);
    
    smarray<LAFloat> wave(5);
    wave.data[0] = 0.05;
    wave.data[1] = 0.121;
    wave.data[2] = 0.29;
    wave.data[3] = 0.65;
    wave.data[4] = 0.35;
    int brightestIndex = 3;
    
    dev.clearAll();
    dev.update();
    dev.pause(initialDelay);
    
    for(int pos = -1*(int)(wave.length-1); pos < (int)base.length; pos++){
        base.fill(initialVal);
        addInRange(base, wave, (ptrdiff_t)pos);
        
        //hold the brightest light on at the end
        if (pos >= numChannels - 4) {
            base.data[numChannels-1] = wave.data[brightestIndex];
        }
        for (int i = 0; i < base.length; i++) {
            dev.setChannel(i, base.data[i]);
        }
        dev.update();
        dev.pause(movementDelay);
    }

    //Made it the end.
    dev.clearAll();
    
    
    int steps = 100;
    LAFloat endBrightness = 0.0;
    //We are really moving along two curves. The first up. The second down.
    AHFloat startT = CubicEaseInOutInverse(wave.data[brightestIndex]);
    AHFloat endT = CubicEaseInOutInverse(1-endBrightness);//Riding this curve backwards

    AHFloat totalTime = (1 - startT) + endT;
    
    LAFloat fractionForUpSteps = (1 - startT)/totalTime;
    int upSteps = steps*fractionForUpSteps;
    int downSteps = steps - upSteps;
    
    AHFloat (*easeFunc)(AHFloat) = &CubicEaseInOut;
    for (int i = 0; i < upSteps; i++) {
        AHFloat brightVal = easingValue(upSteps, i, startT, 1.0, easeFunc);
        dev.setChannel(dev.numChannels()-1, brightVal);
        dev.update();
        dev.pause(glowDelay);
    }
    
    
    LAFloat t = 0;
    LAFloat dt = 1.0 / ( downSteps - 1);
    for(int i = 0; i<downSteps; i++, t += dt){
        dev.setChannel(dev.numChannels()-1, 1 - CubicEaseInOut(t));
        dev.update();
        dev.pause(glowDelay);
    }

    dev.clearAll();
    dev.update();
    dev.pause(finishedDelay);
    
    
}







