//
//  Device.h
//  LightArray
//
//  Created by Spencer Carroll on 8/4/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//


#ifndef DEVICE_H
#define DEVICE_H

#include "Platform.h"
#include "LAConstants.h"

class Device {
    
private:
    Platform * const cPlatform;
    LAFloat mSpeed;
    bool mIsMirrored;
    const int mirrorCorrectedChannel(int channel);
    
public:
    Device(Platform *platform);
    ~Device();
    void setChannel(int channel, LAFloat value);
    void setUpdatePause(int channel, LAFloat value, unsigned long millis);
    LAFloat getChannel(int channel);
    void clearChannel(int channel);
    void clearAll();
    void pause(unsigned long millis);
    void update();
    const int numChannels();
   
    //false is the defualt setting
    bool isMirrored();
    void setMirrored(bool shouldBeMirrored);
    LAFloat getSpeed();
    void setSpeed(LAFloat speed);
    
};


#endif