# LightArray README #

LightArray is a c++ open source project for displaying light shows using dimmable lights. LightArray makes for interesting art and is a great way to visualize algorithms. Check out this youtube [video](youtube.com) to see it deployed on an Arduino.


LightArray currently has two implementations: Arduino and iOS. The iOS implementation is great way to develop for LightArray because it avoids the complications of embedded systems. The iOS implementation is available [here](https://bitbucket.org/spencercarroll/ioslightarray). The Arduino implementation makes beautiful displays and can be found [here](https://bitbucket.org/spencercarroll/arduinolightarray).

LightArray uses gtest for the few tests it has. If you are interested in adding shows feel free to send me a message or email me at: s c a r r o l l 1 5 7 8 @ g m a i l . c o m



